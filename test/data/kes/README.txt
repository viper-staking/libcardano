These test vectors were obtained from: https://github.com/input-output-hk/kes
They were generated from the Cardano Node Haskell code and used as integration test vectors for the Rust implementation.